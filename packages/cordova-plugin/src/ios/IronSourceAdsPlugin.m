#import "IronSourceAdsPlugin.h"
#import "InterstitialAdDelegate.h"
#import "RewardedVideoAdDelegate.h"

@interface IronSourceAdsPlugin ()
@property(nonatomic, strong) RewardedVideoAdDelegate *rewardedVideoDelegate;
@property(nonatomic, strong) InterstitialAdDelegate *interstitialDelegate;
@end

@implementation IronSourceAdsPlugin

#pragma mark - CDVPlugin

- (void)pluginInitialize {

	//    [IronSource sharedInstance];
}

- (void)init:(CDVInvokedUrlCommand *)command {

	NSString *appKey = [command argumentAtIndex:0];
	NSString *userId = [command argumentAtIndex:1];
	BOOL isTestSuite = [command argumentAtIndex:2 withDefault:false];

	//
	// Initialize 'Rewarded Video'
	//
	self.rewardedVideoDelegate =
	    [[RewardedVideoAdDelegate alloc] initWithDelegate:self];
	[IronSource setLevelPlayRewardedVideoDelegate:self.rewardedVideoDelegate];

	//
	// Initialize 'Interstitial'
	//
	self.interstitialDelegate =
	    [[InterstitialAdDelegate alloc] initWithDelegate:self];
	[IronSource setLevelPlayInterstitialDelegate:self.interstitialDelegate];

	[IronSource setUserId:userId];
	[IronSource initWithAppKey:appKey];

	if (isTestSuite) {
		[IronSource setMetaDataWithKey:@"is_test_suite" value:@"enable"];
	}

	CDVPluginResult *result =
	    [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
	[self.commandDelegate sendPluginResult:result
	                            callbackId:command.callbackId];
}

- (void)showRewardedVideo:(CDVInvokedUrlCommand *)command {

	NSString *placementName = [command argumentAtIndex:0];

	if (placementName == nil) {
		[IronSource showRewardedVideoWithViewController:self.viewController];
	} else {
		[IronSource showRewardedVideoWithViewController:self.viewController
		                                      placement:placementName];
	}
	CDVPluginResult *result =
	    [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
	[self.commandDelegate sendPluginResult:result
	                            callbackId:command.callbackId];
}

- (void)getRewardedVideoPlacementInfo:(CDVInvokedUrlCommand *)command {
	NSString *placementName = [command argumentAtIndex:0];
	if (placementName == nil) {
		placementName = @"DefaultRewardedVideo";
	}
	ISPlacementInfo *placementInfo =
	    [IronSource rewardedVideoPlacementInfo:placementName];
	CDVPluginResult *result;
	if (placementInfo != NULL) {
		NSDictionary *data = @{
			@"rewardName" : [placementInfo rewardName],
			@"rewardAmount" : [placementInfo rewardAmount]

		};
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
		                       messageAsDictionary:data];
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
		                           messageAsString:@"placementName_invalid"];
	}
	[self.commandDelegate sendPluginResult:result
	                            callbackId:command.callbackId];
}

- (void)isRewardedVideoPlacementCapped:(CDVInvokedUrlCommand *)command {
	NSString *placementName = [command argumentAtIndex:0];
	if (placementName == nil) {
		placementName = @"DefaultRewardedVideo";
	}
	CDVPluginResult *result;
	BOOL capped = [IronSource isRewardedVideoCappedForPlacement:placementName];
	if (capped) {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
		                           messageAsString:@"capped"];
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
		                           messageAsString:@"ok"];
	}
	[self.commandDelegate sendPluginResult:result
	                            callbackId:command.callbackId];
}

- (void)loadInterstitial:(CDVInvokedUrlCommand *)command {
	[IronSource loadInterstitial];
	CDVPluginResult *result =
	    [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
	[self.commandDelegate sendPluginResult:result
	                            callbackId:command.callbackId];
}

- (void)isInterstitialReady:(CDVInvokedUrlCommand *)command {
	BOOL isReady = [IronSource hasInterstitial];
	CDVPluginResult *result;
	if (isReady) {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
	}
	[self.commandDelegate sendPluginResult:result
	                            callbackId:command.callbackId];
}

- (void)isRewardedVideoAvailable:(CDVInvokedUrlCommand *)command {
	BOOL isReady = [IronSource hasRewardedVideo];
	CDVPluginResult *result;
	if (isReady) {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
		                           messageAsString:@"available"];

	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
		                           messageAsString:@"not_available"];
	}
	[self.commandDelegate sendPluginResult:result
	                            callbackId:command.callbackId];
}

- (void)showInterstitial:(CDVInvokedUrlCommand *)command {

	[IronSource showInterstitialWithViewController:self.viewController];
	CDVPluginResult *result =
	    [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
	[self.commandDelegate sendPluginResult:result
	                            callbackId:command.callbackId];
}

- (void)getInterstitialPlacementInfo:(CDVInvokedUrlCommand *)command {
	NSString *placementName = [command argumentAtIndex:0];
	if (placementName == nil) {
		placementName = @"DefaultInterstitial";
	}
	ISPlacementInfo *placementInfo =
	    [IronSource rewardedVideoPlacementInfo:placementName];
	CDVPluginResult *result;
	if (placementInfo != NULL) {
		NSDictionary *data = @{
			@"placementName" : [placementInfo placementName]

		};
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
		                       messageAsDictionary:data];
	} else {
		result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
		                           messageAsString:@"placementName_invalid"];
	}
	[self.commandDelegate sendPluginResult:result
	                            callbackId:command.callbackId];
}

- (void)validateIntegration:(CDVInvokedUrlCommand *)command {
	[self.commandDelegate runInBackground:^{
	  [ISIntegrationHelper validateIntegration];
	  CDVPluginResult *result =
		  [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
	  [self.commandDelegate sendPluginResult:result
		                          callbackId:command.callbackId];
	}];
}

- (void)launchTestSuite:(CDVInvokedUrlCommand *)command {
	[self.commandDelegate runInBackground:^{
	  [IronSource launchTestSuite:self.viewController];
	  CDVPluginResult *result =
		  [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
	  [self.commandDelegate sendPluginResult:result
		                          callbackId:command.callbackId];
	}];
}

- (void)setDynamicUserId:(CDVInvokedUrlCommand *)command {
	NSString *userId = [command argumentAtIndex:0];
	[IronSource setDynamicUserId:userId];
	CDVPluginResult *result =
	    [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
	[self.commandDelegate sendPluginResult:result
	                            callbackId:command.callbackId];
}

- (void)fireEvent:(NSString *)event {
	NSString *js =
	    [NSString stringWithFormat:@"cordova.fireWindowEvent('%@')", event];
	[self.commandDelegate evalJs:js];
}

- (void)fireEvent:(NSString *)event withData:(NSDictionary *)data {
	NSError *error = nil;
	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
	                                                   options:kNilOptions
	                                                     error:&error];

	NSString *jsonString = [[NSString alloc] initWithData:jsonData
	                                             encoding:NSUTF8StringEncoding];
	NSString *js =
	    [NSString stringWithFormat:@"cordova.fireWindowEvent('%@', %@)", event,
	                               jsonString];
	[self.commandDelegate evalJs:js];
}

@end
