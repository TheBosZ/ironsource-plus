#import "Constants.h"
#import "IronSourceAdsPlugin.h"
#import <Foundation/Foundation.h>
#import <IronSource/IronSource.h>

NS_ASSUME_NONNULL_BEGIN

@interface InterstitialAdDelegate : NSObject <LevelPlayInterstitialDelegate>

@property(weak, nonatomic) IronSourceAdsPlugin *delegate;

- (instancetype)initWithDelegate:(IronSourceAdsPlugin *)delegate;

@end

NS_ASSUME_NONNULL_END
