#import "InterstitialAdDelegate.h"

@implementation InterstitialAdDelegate

- (instancetype)initWithDelegate:(IronSourceAdsPlugin *)delegate {
	self = [super init];

	if (self) {
		_delegate = delegate;
	}

	return self;
}

/**
 Called after an interstitial has been loaded
 @param adInfo The info of the ad.
 */
- (void)didLoadWithAdInfo:(ISAdInfo *)adInfo {
	NSDictionary *data = @{@"available" : @TRUE};
	[self.delegate fireEvent:EVENT_INTERSTITIAL_DID_LOAD withData:data];
}

/**
 Called after an interstitial has attempted to load but failed.
 @param error The reason for the error
 */
- (void)didFailToLoadWithError:(NSError *)error {
	NSDictionary *data = @{
		@"error" :
		    @{@"errorCode" : @(error.code), @"errorMessage" : error.description}
	};

	[self.delegate fireEvent:EVENT_INTERSTITIAL_LOAD_FAILED withData:data];
}

/**
 Called after an interstitial has been opened.
 This is the indication for impression.
 @param adInfo The info of the ad.
 */
- (void)didOpenWithAdInfo:(ISAdInfo *)adInfo {
	[self.delegate fireEvent:EVENT_INTERSTITIAL_OPENED];
}

/**
 Called after an interstitial has been displayed on the screen.
 This callback is not supported by all networks, and we recommend using it
 only if it's supported by all networks you included in your build.
 @param adInfo The info of the ad.
 */
- (void)didShowWithAdInfo:(ISAdInfo *)adInfo {
	[self.delegate fireEvent:EVENT_INTERSTITIAL_SHOWN];
}

/**
 Called after an interstitial has attempted to show but failed.
 @param error The reason for the error.
 @param adInfo The info of the ad.
 */
- (void)didFailToShowWithError:(NSError *)error andAdInfo:(ISAdInfo *)adInfo {
	NSDictionary *data = @{
		@"error" :
		    @{@"errorCode" : @(error.code), @"errorMessage" : error.description}
	};

	[self.delegate fireEvent:EVENT_INTERSTITIAL_LOAD_FAILED withData:data];
}

/**
 Called after an interstitial has been clicked.
 @param adInfo The info of the ad.
 */
- (void)didClickWithAdInfo:(ISAdInfo *)adInfo {
	[self.delegate fireEvent:EVENT_INTERSTITIAL_CLICKED];
}

/**
 Called after an interstitial has been dismissed.
 @param adInfo The info of the ad.
 */
- (void)didCloseWithAdInfo:(ISAdInfo *)adInfo {
	[self.delegate fireEvent:EVENT_INTERSTITIAL_CLOSED];
}

@end
