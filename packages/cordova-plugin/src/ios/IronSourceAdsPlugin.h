#import "Constants.h"
#import "IronSource/IronSource.h"
#import <Cordova/CDV.h>
#import <Foundation/Foundation.h>

@interface IronSourceAdsPlugin : CDVPlugin

- (void)init:(CDVInvokedUrlCommand *)command;

- (void)showRewardedVideo:(CDVInvokedUrlCommand *)command;

- (void)getRewardedVideoPlacementInfo:(CDVInvokedUrlCommand *)command;

- (void)isRewardedVideoPlacementCapped:(CDVInvokedUrlCommand *)command;

- (void)isRewardedVideoAvailable:(CDVInvokedUrlCommand *)command;

- (void)loadInterstitial:(CDVInvokedUrlCommand *)command;

- (void)isInterstitialReady:(CDVInvokedUrlCommand *)command;

- (void)getInterstitialPlacementInfo:(CDVInvokedUrlCommand *)command;

- (void)showInterstitial:(CDVInvokedUrlCommand *)command;

- (void)setDynamicUserId:(CDVInvokedUrlCommand *)command;

- (void)validateIntegration:(CDVInvokedUrlCommand *)command;

- (void)launchTestSuite:(CDVInvokedUrlCommand *)command;

- (void)fireEvent:(NSString *)event;
- (void)fireEvent:(NSString *)event withData:(NSDictionary *)data;
@end
