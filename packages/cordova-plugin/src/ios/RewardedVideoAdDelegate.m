#import "RewardedVideoAdDelegate.h"

@implementation RewardedVideoAdDelegate

- (instancetype)initWithDelegate:(IronSourceAdsPlugin *)delegate {
    self = [super init];
    
    if (self) {
        _delegate = delegate;
    }
    
    return self;
}

/**
 Called after a rewarded video has changed its availability to true.
 @param adInfo The info of the ad.
 */
- (void)hasAvailableAdWithAdInfo:(ISAdInfo *)adInfo {
	NSDictionary *data = @{
			@"available" : @(true)
	};
	[self.delegate fireEvent:EVENT_REWARDED_VIDEO_AVAILABILITY_CHANGED withData:data];
    
}

/**
 Called after a rewarded video has changed its availability to false.
 */
- (void)hasNoAvailableAd {
	NSDictionary *data = @{
			@"available" : @(false)
	};
	[self.delegate fireEvent:EVENT_REWARDED_VIDEO_AVAILABILITY_CHANGED withData:data];
}

/**
 Called after a rewarded video has been opened.
 @param adInfo The info of the ad.
 */
- (void)didOpenWithAdInfo:(ISAdInfo *)adInfo {
	[self.delegate fireEvent:EVENT_REWARDED_VIDEO_OPENED];
}

/**
 Called after a rewarded video has attempted to show but failed.
 @param error The reason for the error.
 @param adInfo The info of the ad.
 */
- (void)didFailToShowWithError:(NSError *)error
                     andAdInfo:(ISAdInfo *)adInfo {
	NSDictionary *data = @{
		@"error": @{
				@"errorCode" : @(error.code),
				@"errorMessage" : error.description
		}
	};
	
	[self.delegate fireEvent:EVENT_REWARDED_VIDEO_FAILED withData: data];
}

/**
 Called after a rewarded video has been clicked.
 This callback is not supported by all networks, and we recommend using it 
 only if it's supported by all networks you included in your build
 @param placementInfo An object that contains the placement's reward name and amount.
 @param adInfo The info of the ad.
 */ 
- (void)didClick:(ISPlacementInfo *)placementInfo
      withAdInfo:(ISAdInfo *)adInfo {
    //Squelch, we don't support it
}

/**
 Called after a rewarded video has been viewed completely and the user is eligible for a reward.
 @param placementInfo An object that contains the placement's reward name and amount.
 @param adInfo The info of the ad.
 */
- (void)didReceiveRewardForPlacement:(ISPlacementInfo *)placementInfo
                          withAdInfo:(ISAdInfo *)adInfo {
	NSDictionary *data = @{
			@"placement": @{
					@"placementName": placementInfo.placementName,
					@"rewardName": placementInfo.rewardName,
					@"rewardAmount": placementInfo.rewardAmount
			}
	};
	[self.delegate fireEvent:EVENT_REWARDED_VIDEO_REWARDED withData:data];
}

/**
 Called after a rewarded video has been dismissed.
 @param adInfo The info of the ad.
 */
- (void)didCloseWithAdInfo:(ISAdInfo *)adInfo {
	[self.delegate fireEvent:EVENT_REWARDED_VIDEO_CLOSED];
}

@end
