package com.deineagentur.cordova.plugin.ironsource;

import com.ironsource.mediationsdk.adunit.adapter.utility.AdInfo;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.sdk.LevelPlayRewardedVideoListener;

import org.json.JSONException;
import org.json.JSONObject;

public class RewardedAdListener implements LevelPlayRewardedVideoListener {
	private final IronSourceAdsPlugin plugin;

	public RewardedAdListener(IronSourceAdsPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public void onAdAvailable(AdInfo adInfo) {
		final JSONObject data = new JSONObject();
		try {
			data.put("videoAvailable", true);
		} catch (JSONException e) {
		}
		this.plugin.fireEvent("onRewardedVideoAvailabilityChanged", data);
	}

	@Override
	public void onAdUnavailable() {
		final JSONObject data = new JSONObject();
		try {
			data.put("videoAvailable", false);
		} catch (JSONException e) {
		}
		this.plugin.fireEvent("onRewardedVideoAvailabilityChanged", data);
	}

	@Override
	public void onAdOpened(AdInfo adInfo) {
		this.plugin.fireEvent("onRewardedVideoAdOpened");
	}

	@Override
	public void onAdShowFailed(IronSourceError ironSourceError, AdInfo adInfo) {
		final JSONObject data = new JSONObject();
		try {
			data.put("errorCode", ironSourceError.getErrorCode());
			data.put("errorMessage", ironSourceError.getErrorMessage());
		} catch (JSONException e) {
		}
		this.plugin.fireEvent("onRewardedVideoAdShowFailed", data);
	}

	@Override
	public void onAdClicked(Placement placement, AdInfo adInfo) {
        this.plugin.fireEvent("onRewardedVideoAdClicked", getPlacementObject(placement));
	}

	@Override
	public void onAdRewarded(Placement placement, AdInfo adInfo) {
        this.plugin.fireEvent("onRewardedVideoAdRewarded", getPlacementObject(placement));
	}

	@Override
	public void onAdClosed(AdInfo adInfo) {
        this.plugin.fireEvent("onRewardedVideoAdClosed");
	}

	private JSONObject getPlacementObject(Placement placement) {
		final JSONObject data = new JSONObject();
		final JSONObject outer = new JSONObject();
		try {
			data.put("placementName", placement.getPlacementName());
			data.put("rewardName", placement.getRewardName());
			data.put("rewardAmount", placement.getRewardAmount());
			outer.put("placement", data);
		} catch (JSONException e) {
		}
		return outer;
	}
}
