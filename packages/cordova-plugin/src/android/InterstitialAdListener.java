package com.deineagentur.cordova.plugin.ironsource;

import com.ironsource.mediationsdk.adunit.adapter.utility.AdInfo;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.sdk.LevelPlayInterstitialListener;

import org.json.JSONException;
import org.json.JSONObject;

public class InterstitialAdListener implements LevelPlayInterstitialListener {
	private final IronSourceAdsPlugin plugin;

	public InterstitialAdListener(IronSourceAdsPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public void onAdReady(AdInfo adInfo) {
		this.plugin.fireEvent("onInterstitialAdReady");
	}

	@Override
	public void onAdLoadFailed(IronSourceError ironSourceError) {
		final JSONObject data = new JSONObject();
		try {
			data.put("errorCode", ironSourceError.getErrorCode());
			data.put("errorMessage", ironSourceError.getErrorMessage());
		} catch (JSONException e) {
		}
		this.plugin.fireEvent("onInterstitialAdLoadFailed", data);
	}

	@Override
	public void onAdOpened(AdInfo adInfo) {
		this.plugin.fireEvent("onInterstitialAdOpened");
	}

	@Override
	public void onAdShowSucceeded(AdInfo adInfo) {
		this.plugin.fireEvent("onInterstitialAdShowSucceeded");
	}

	@Override
	public void onAdShowFailed(IronSourceError ironSourceError, AdInfo adInfo) {
		final JSONObject data = new JSONObject();
		try {
			data.put("errorCode", ironSourceError.getErrorCode());
			data.put("errorMessage", ironSourceError.getErrorMessage());
		} catch (JSONException e) {
		}
		this.plugin.fireEvent("onInterstitialAdShowFailed", data);
	}

	@Override
	public void onAdClicked(AdInfo adInfo) {
		this.plugin.fireEvent("onInterstitialAdClicked");
	}

	@Override
	public void onAdClosed(AdInfo adInfo) {
		this.plugin.fireEvent("onInterstitialAdClosed");
	}
}
