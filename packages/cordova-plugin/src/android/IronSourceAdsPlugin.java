package com.deineagentur.cordova.plugin.ironsource;

import android.app.Activity;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import com.ironsource.adapters.supersonicads.SupersonicConfig;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.integration.IntegrationHelper;
import com.ironsource.mediationsdk.model.InterstitialPlacement;
import com.ironsource.mediationsdk.model.Placement;

public class IronSourceAdsPlugin extends CordovaPlugin {
	private static final String TAG = "[IronSourceAdsPlugin]";

	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
		Log.d(TAG, "Initializing IronSourceAdsPlugin");
	}

	@Override
	public void onPause(boolean multitasking) {
		super.onPause(multitasking);
		Log.d(TAG, "onPause");
		IronSource.onPause(this.cordova.getActivity());
	}

	@Override
	public void onResume(boolean multitasking) {
		super.onResume(multitasking);
		Log.d(TAG, "onResume");
		IronSource.onResume(this.cordova.getActivity());
	}

	@Override
	public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
		Log.d(TAG, "Execute: " + action);
		String placementName;
		String userId;
		switch (action) {
		case "init":
			final String appKey = args.getString(0);
			userId = args.getString(1);
			final boolean enableTestSuite = args.optBoolean(2, false);
			final IronSourceAdsPlugin vthis = this;
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					vthis.init(appKey, userId, enableTestSuite);
					callbackContext.success();
				}
			});
			return true;
		case "showRewardedVideo":
			placementName = "DefaultRewardedVideo";
			if (args.length() == 1) {
				placementName = args.getString(0);
			}
			return this.showRewardedVideo(placementName);
		case "getRewardedVideoPlacementInfo":
			placementName = "DefaultRewardedVideo";
			if (args.length() == 1) {
				placementName = args.getString(0);
			}
			return this.getRewardedVideoPlacementInfo(placementName, callbackContext);
		case "isRewardedVideoPlacementCapped":
			placementName = "DefaultRewardedVideo";
			if (args.length() == 1) {
				placementName = args.getString(0);
			}
			return this.isRewardedVideoPlacementCapped(placementName, callbackContext);
		case "isRewardedVideoAvailable":
			return this.isRewardedVideoAvailable(callbackContext);
		case "setDynamicUserId":
			userId = args.getString(0);
			return this.setDynamicUserId(userId);
		case "loadInterstitial":
			return this.loadInterstitial(callbackContext);
		case "isInterstitialReady":
			return this.isInterstitialReady(callbackContext);
		case "getInterstitialPlacementInfo":
			placementName = "DefaultInterstitial";
			if (args.length() == 1) {
				placementName = args.getString(0);
			}
			return this.getInterstitialPlacementInfo(placementName, callbackContext);
		case "validateIntegration":
			Activity activity = this.cordova.getActivity();
			cordova.getThreadPool().execute(new Runnable() {
				public void run() {
					IntegrationHelper.validateIntegration(activity);
					callbackContext.success();
				}
			});
			return true;
		case "showInterstitial":
			placementName = "DefaultInterstitial";
			if (args.length() == 1) {
				placementName = args.getString(0);
			}
			return this.showInterstitial(placementName);
		case "launchTestSuite":
			cordova.getThreadPool().execute(new Runnable() {
				public void run () {
					IronSource.launchTestSuite(cordova.getContext());
				}
			});
			return true;
		}
		return false;
	}

	void fireEvent(final String event) {
		final CordovaWebView view = this.webView;
		this.cordova.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				view.loadUrl("javascript:cordova.fireWindowEvent('" + event + "');");
			}
		});
	}

	void fireEvent(final String event, final JSONObject data) {
		final CordovaWebView view = this.webView;
		this.cordova.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				view.loadUrl(String.format("javascript:cordova.fireWindowEvent('%s', %s);", event, data.toString()));
			}
		});
	}

	private boolean init(String appKey, String userId, boolean enableTestSuite) {
		IronSource.setLevelPlayRewardedVideoListener(new RewardedAdListener(this));

		SupersonicConfig.getConfigObj().setClientSideCallbacks(true);
		IronSource.setLevelPlayInterstitialListener(new InterstitialAdListener(this));
		IronSource.setUserId(userId);
		if (enableTestSuite) {
			IronSource.setMetaData("is_test_suite", "enable");
		}

		IronSource.init(this.cordova.getActivity(), appKey);
		return true;
	}

	private boolean showRewardedVideo(String placementName) {
		IronSource.showRewardedVideo(placementName);
		return true;
	}

	private boolean getRewardedVideoPlacementInfo(String placementName, CallbackContext callbackContext) {
		Placement placement = IronSource.getRewardedVideoPlacementInfo(placementName);
		if (placement != null) {
			JSONObject event = new JSONObject();
			try {
				event.put("rewardName", placement.getRewardName());
				event.put("rewardAmount", placement.getRewardAmount());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			callbackContext.success(event);
		} else {
			callbackContext.error("placementName_invalid");
		}
		return true;
	}

	private boolean isRewardedVideoPlacementCapped(String placementName, CallbackContext callbackContext) {
		boolean isCapped = IronSource.isRewardedVideoPlacementCapped(placementName);
		if (isCapped) {
			callbackContext.error("capped");
		} else {
			callbackContext.success("ok");
		}
		return true;
	}

	private boolean isRewardedVideoAvailable(CallbackContext callbackContext) {
		boolean isAvailable = IronSource.isRewardedVideoAvailable();
		if (isAvailable) {
			callbackContext.success("available");
		} else {
			callbackContext.error("not_available");
		}
		return true;
	}

	private boolean setDynamicUserId(String userId) {
		IronSource.setDynamicUserId(userId);
		return true;
	}

	private boolean loadInterstitial(CallbackContext callbackContext) {
		IronSource.loadInterstitial();
		callbackContext.success("ok");
		return true;
	}

	private boolean isInterstitialReady(CallbackContext callbackContext) {
		boolean isReady = IronSource.isInterstitialReady();
		if (isReady) {
			callbackContext.success("ready");
		} else {
			callbackContext.error("not_ready");
		}
		return true;
	}

	private boolean getInterstitialPlacementInfo(String placementName, CallbackContext callbackContext) {
		InterstitialPlacement placement = IronSource.getInterstitialPlacementInfo(placementName);
		if (placement != null) {
			JSONObject event = new JSONObject();
			try {
				event.put("placementName", placement.getPlacementName());
				event.put("placementId", placement.getPlacementId());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			callbackContext.success(event);
		} else {
			callbackContext.error("placementName_invalid");
		}
		return true;
	}

	private boolean showInterstitial(String placementName) {
		IronSource.showInterstitial(placementName);
		return true;
	}
}
