## 1.3.4

- Update IronSource SDK to 8.3.0
- Add Moloco plugin
- Add Yandex plugin (see known bugs section)
- Add [Test Suite](https://developers.is.com/ironsource-mobile/android/unity-levelplay-test-suite/) option
- **BREAKING CHANGE**: Remove offerwall as it is no longer available
- Update all non-deprecated plugins to latest versions

Known bugs:

- Yandex plugin causes compilation error on iOS
- MyTarget plugin causes compilation error on iOS

## 1.3.3

- Update IronSource SDK to 7.6.0

## 1.3.2

- Update Android IronSource SDK to 7.5.1
- Update gradle files to be more friendly to Capacitor
- Add BidMachine plugin

## 1.3.1

- Update IronSource SDK to 7.5.0.0
- Update all non-deprecated plugins to latest versions

## 1.3.0

- Update IronSource SDK to 7.3.0.1
- Add APS, CSJ, Mintegral, SuperAwesome, Tencent and Yahoo plugins
- Update all non-deprecated plugins to latest versions

## 1.2.1

-   Switch to Monorepo
-   Version numbers now are all in lock-step
-   Update IronSource SDK to 7.1.6.0
-   Update all Android plugins because bintray is going away
-   Add SkAdNetwork for iOS plugins
-   Update all plugins to latest versions

## 0.3.0

-   Updated IronSource SDK to 7.1.5
-   Validate runs on a thread to stop Cordova from complaining
