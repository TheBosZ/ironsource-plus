function IronSourceAds(appKey, userId, successCallback, enableTestSuite) {
	cordova.exec(successCallback, null, 'IronSourceAdsPlugin', 'init', [appKey, userId, enableTestSuite]);
	this.showRewardedVideo = function (placementName, successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'IronSourceAdsPlugin', 'showRewardedVideo', [placementName]);
	};
	this.getRewardedVideoPlacementInfo = function (placementName, successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'IronSourceAdsPlugin', 'getRewardedVideoPlacementInfo', [placementName]);
	};
	this.isRewardedVideoPlacementCapped = function (placementName, successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'IronSourceAdsPlugin', 'isRewardedVideoPlacementCapped', [placementName]);
	};
	this.isRewardedVideoAvailable = function (successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'IronSourceAdsPlugin', 'isRewardedVideoAvailable', []);
	};
	this.validateIntegration = function (successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'IronSourceAdsPlugin', 'validateIntegration', []);
	};
	this.launchTestSuite = function (successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'IronSourceAdsPlugin', 'launchTestSuite', []);
	};
	this.setDynamicUserId = function (userId, successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'IronSourceAdsPlugin', 'setDynamicUserId', [userId]);
	};
	this.loadInterstitial = function (successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'IronSourceAdsPlugin', 'loadInterstitial', []);
	};
	this.isInterstitialReady = function (successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'IronSourceAdsPlugin', 'isInterstitialReady', []);
	};
	this.getInterstitialPlacementInfo = function (placementName, successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'IronSourceAdsPlugin', 'getInterstitialPlacementInfo', [placementName]);
	};
	this.showInterstitial = function (placementName, successCallback, failureCallback) {
		cordova.exec(successCallback, failureCallback, 'IronSourceAdsPlugin', 'showInterstitial', [placementName]);
	};
}

if (typeof module !== undefined && module.exports) {

	module.exports = IronSourceAds;
}
