# IronSource Ads Cordova Plugin

Add support for [IronSource Ads](https://www.ironsrc.com/) to your Cordova and Phonegap based mobile apps.

## History

This is a fork of [cordova-plugin-ironSource](https://github.com/Dexus/cordova-plugin-ironSource). It aims to be kept up to date as well as enable ad mediation. It is planned to be a drop-in replacement and will retain the same api of that plugin.

#### Currently supports ironSource 8.3.0 (for both iOS and Android)

## How do I install it?

Install using `cordova`:

```
cordova plugin add @ironsource-plus/cordova-plugin
```

## How do I use it?

First, you'll need to construct a new instance of the plugin using `new IronSourceAds`.

The constructor can take 2-4 arguments. The first one is your AppKey from IronSource. The second is a user Identifier. This can be anything and is described [in the IronSource docs](https://developers.is.com/ironsource-mobile/android/additional-sdk-settings-android/#step-1).

The last two are optional. A callback to be ran after the plugin is initialized and true/false to enable the test suite (default false);

```javascript
document.addEventListener('deviceready', function() {

	var isAds = new IronSourceAds("app_key", "some_unique_userid", function(){console.log('Init has finished');}, false);

	// show a rewarded ad
	isAds.showRewardedAd();

	// show a rewarded ad for placement RightHere
	isAds.showRewardedAd("RightHere");

    // show an interstitial
    isAds.showInterstitial();

    // launch in Android Studio/Xcode mediation integration verification
    isAds.validateIntegration();

    // Check is a rewarded video is available to show
    isAds.isRewardedVideoAvailable(function () {
    	alert('Yes');
	}, function () {
		alert('No');
	});

	// Check is an interstitial is available to show
    isAds.isInterstitialAdAvailable(function () {
    	alert('Yes');
	}, function () {
		alert('No');
	});

	// reward your users
	window.addEventListener("onRewardedVideoAdClosed", function(e) {

		var placement = e.placement;
		console.log(placement.placementName);
		console.log(placement.rewardName);
		console.log(placement.rewardAmount);
	}, false);

}, false);
```

## Can I just see a working example?

Yep. Check out the [demo project](https://bitbucket.org/TheBosZ/ironsource-plus/src/master/externals/example-app/). It runs on both Android and iOS.

## What events are supported?

### Interstitial

1. onInterstitialAdClicked
1. onInterstitialAdClosed
1. onInterstitialAdLoadFailed
1. onInterstitialAdOpened
1. onInterstitialAdReady
1. onInterstitialAdShowFailed
1. onInterstitialAdShowSucceeded

### Rewarded Video

1. onRewardedVideoAdClosed
1. onRewardedVideoAdOpened
1. onRewardedVideoAdRewarded
1. onRewardedVideoAdShowFailed
1. onRewardedVideoAvailabilityChanged
1. onRewardedVideoAdClicked
