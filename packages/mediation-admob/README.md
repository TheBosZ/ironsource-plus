## Installation

The Admob mediation plugin needs your App ID, find it and install like this:

```
cordova plugin add https://bitbucket.org/TheBosZ/cordova-plugin-ironsource-plus-mediation-admob --variable ANDROID_APP_ID=ca-app-pub-xxx~xxx --variable IOS_APP_ID=ca-app-pub-xxx~xxx
```
 
 If you fail to do this step, you app will hard crash when you try to run it.
