/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
	var APP_KEY = '85460dcd';

	console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);

	var btn_rewarded = document.getElementById('btn_rewarded');
	var btn_initialize_interstitial = document.getElementById('btn_initial_interstitial');
	var btn_show_interstitial = document.getElementById('btn_show_interstitial');
	var btn_validate = document.getElementById('btn_validate');
	var btn_testSuite = document.getElementById('btn_testSuite');
	var isAds = new IronSourceAds(APP_KEY, 'demo_' + Date.now(), undefined, true);
	updateButtonState();

	btn_rewarded.addEventListener('click', function () {
		isAds.isRewardedVideoAvailable(function () {
			isAds.showRewardedVideo();
		}, function () {
			console.info('rewarded video not avilable');
		});
	}, false);

	btn_validate.addEventListener('click', function () {
		isAds.validateIntegration();
	}, false);

	btn_testSuite.addEventListener('click', function () {
		isAds.launchTestSuite();
	}, false);

	btn_initialize_interstitial.addEventListener('click', function () {
		isAds.loadInterstitial();
	}, false);

	btn_show_interstitial.addEventListener('click', function () {
		isAds.isInterstitialReady(function () {
			isAds.showInterstitial();
		}, function () {
			console.info('interstitial not available');
		});
	}, false);

	var rewardPlacement;
	window.addEventListener('onRewardedVideoAdRewarded', function (e) {
		var placement = e.placement;
		if (!placement) {
			return;
		}
		console.info("rewarded", placement);
		rewardPlacement = placement;
	}, false);

	window.addEventListener('onRewardedVideoAdClosed', function () {
		if (rewardPlacement) {
			alert("Video Rewarded: You have been rewarded " + rewardPlacement.rewardAmount + " " + rewardPlacement.rewardName);
			rewardPlacement = null;
		}
	}, false);

	window.addEventListener('onRewardedVideoAvailabilityChanged', function () {
		handleVideoButtonState();
	}, false);

	window.addEventListener('onInterstitialAdReady', function () {
		handleInterstitialShowButtonState();
	}, false);

	window.addEventListener('onInterstitialAdClosed', function () {
		handleInterstitialShowButtonState();
	}, false);

	function updateButtonState() {
		handleVideoButtonState();
	}

	function handleVideoButtonState() {
		isAds.isRewardedVideoAvailable(function () {
			btn_rewarded.innerText = 'Show Rewarded Video';
			btn_rewarded.disabled = false;
		}, function () {
			btn_rewarded.innerText = 'Initializing Rewarded Video';
			btn_rewarded.disabled = true;
		});
	}

	function handleInterstitialShowButtonState() {
		isAds.isInterstitialReady(function () {
			btn_show_interstitial.disabled = false;
		}, function () {
			btn_show_interstitial.disabled = true;
		});
	}
}
