const util = require('util');
const exec = util.promisify(require('child_process').exec);

const ADMOB_APP_ID = 'ca-app-pub-3940256099942544~3347511713';

const skipDeprecated = [
	'mediation-adcolony',
	'mediation-amazon',
	'mediation-snap',
	'mediation-tapjoy',
	'mediation-yahoo',
];

const skipIos = [
	'mediation-mytarget',
	'mediation-yandex',
];

var pluginNames = [
	'cordova-plugin',
	'mediation-adcolony', //deprecated
	`mediation-admob --variable ANDROID_APP_ID=${ADMOB_APP_ID} --variable IOS_APP_ID=${ADMOB_APP_ID}`,
	'mediation-amazon', //deprecated
	'mediation-aps',
	'mediation-applovin',
	'mediation-bidmachine',
	'mediation-chartboost',
	'mediation-csj',
	'mediation-facebook',
	'mediation-fyber',
	'mediation-hyprmx',
	'mediation-inmobi',
	'mediation-maio',
	'mediation-mintegral',
	'mediation-moloco',
	'mediation-mytarget', //Errors on iOS
	'mediation-pangle',
	'mediation-smaato',
	'mediation-snap', //deprecated
	'mediation-superawesome',
	'mediation-tapjoy', //deprecated
	'mediation-tencent',
	'mediation-unityads',
	'mediation-vungle',
	'mediation-yandex', //has errors in swift code
	'mediation-yahoo', //deprecated
];

const useDeprecated = process.argv.includes('deprecated');
const useIos = process.argv.includes('ios');

async function remove() {
	for (const plugin of pluginNames.reverse()) {
		try {
			await exec(`cordova plugin rm @ironsource-plus/${plugin}`);
		} catch (error) {
			if (!error.stderr.includes('is not present in the project')) { //don't care about this error as it's not actually an error
				console.error(error);
			}
		}
	}
}

async function add() {
	for (const plugin of pluginNames) {
		if (!useDeprecated) {
			if (skipDeprecated.includes(plugin)) {
				continue;
			}
		}

		if (useIos) {
			if (skipIos.includes(plugin)) {
				continue;
			}
		}

		try {
			await exec(`cordova plugin add file:../../packages/${plugin}`);
		} catch (error) {
			console.error(error);
		}
	}
}

if (process.argv.includes('add')) {
	add();
}
if (process.argv.includes('remove')) {
	remove();
}
