import 'package:ironsource_plus_mediation_creator/gradle_builder.dart';
import 'package:ironsource_plus_mediation_creator/mediation_meta.dart';
import 'package:ironsource_plus_mediation_creator/package_json_builder.dart';
import 'package:ironsource_plus_mediation_creator/plugin_xml_builder.dart';

class BuiltStrings {
	final String packageJson;
	final String pluginXML;
	final String gradle;

	BuiltStrings(this.packageJson, this.pluginXML, this.gradle);
}

BuiltStrings getAllStrings(MediationMeta meta, String sdkVersion) {
	return BuiltStrings(buildPackageJSONString(meta), buildPluginXMLString(meta), meta.isAndroid ? buildGradleString(meta, sdkVersion) : null);
}

