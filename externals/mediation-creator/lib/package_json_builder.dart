import 'package:ironsource_plus_mediation_creator/mediation_meta.dart';

String buildPackageJSONString(MediationMeta meta) {
	var packageLower = meta.name.toLowerCase();
	var platforms = [];
	var platformKeywords = [];
	
	if (meta.isAndroid) {
		platforms.add('android');
		platformKeywords.add('cordova-android');
	}
	if (meta.isIos) {
		platforms.add('ios');
		platformKeywords.add('cordova-ios');
	}

	var platformsString = platforms.map((entry) => '"$entry"').join(',\n\t\t\t');
	var platformKeywordsString = platformKeywords.map((entry) => '"$entry"').join(',\n\t\t');
	return '''
{
	"name": "@ironsource-plus/mediation-$packageLower",
	"version": "${meta.version}",
	"description": "IronSource Mediation Adapter for ${meta.name}",
	"cordova": {
		"id": "@ironsource-plus/mediation-$packageLower",
		"platforms": [
			$platformsString
		]
	},
	"repository": {
		"type": "git",
		"url": "git+https://bitbucket.org/TheBosZ/ironsource-plus.git"
	},
	"keywords": [
		"ecosystem:cordova",
		"cordova",
		"plugin",
		"ad",
		"ironsource",
		"$packageLower",
		"mediation",
		$platformKeywordsString
	],
	"author": {
		"name": "Nathan Kerr",
		"url": "https://fallingdeathgames.com"
	},
	"license": "MIT",
	"bugs": {
		"url": "https://bitbucket.org/TheBosZ/ironsource-plus/issues"
	},
	"homepage": "https://bitbucket.org/TheBosZ/ironsource-plus"
}
''';
}
