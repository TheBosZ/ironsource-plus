import 'package:ironsource_plus_mediation_creator/mediation_meta.dart';

String buildPluginXMLString(MediationMeta meta, [String mainVersion = '0.2.0']) {
	var packageLower = meta.name.toLowerCase();
	
	return '''
<?xml version="1.0" encoding="UTF-8"?>

<plugin 
	xmlns="http://apache.org/cordova/ns/plugins/1.0" 
	id="@ironsource-plus/mediation-$packageLower" 
	version="${meta.version}"
>

	<name>IronSource Mediation Adapter for ${meta.name}</name>
	<description>IronSource Mediation Adapter for ${meta.name}</description>
	<author>Nathan Kerr</author>
	<keywords>ironsource,mediation,$packageLower</keywords>
	<license>MIT</license>

	<dependency 
		id="@ironsource-plus/cordova-plugin" 
		version=">=$mainVersion" 
	/>

${meta.isAndroid ? getAndroidSnippet(meta) : ''}${meta.isIos ? getIosSnippet(meta) : ''}</plugin>
''';
}

String getAndroidSnippet(MediationMeta meta) {
	var packageLower = meta.name.toLowerCase();
	var isAdmob = packageLower == 'admob';

	var androidAdmobDetails = '''

		<preference name="ANDROID_APP_ID" />
		<config-file 
			target="./res/values/strings.xml" 
			parent="/resources"
		>
			<string name="isplus_android_app_id">\$ANDROID_APP_ID</string>
		</config-file>
		<config-file 
			target="AndroidManifest.xml" 
			parent="/manifest/application"
		>
			<meta-data android:name="com.google.android.gms.ads.APPLICATION_ID" android:value="@string/isplus_android_app_id" />
			<meta-data android:name="com.google.android.gms.version" android:value="@integer/google_play_services_version" />
		</config-file>
''';

	return '''
	<platform name="android">${isAdmob ? androidAdmobDetails : ''}
		<framework 
			src="src/android/adapter.gradle" 
			custom="true" 
			type="gradleReference" 
		/>
	</platform>
''';
}

String getIosSnippet(MediationMeta meta) {
	var packageLower = meta.name.toLowerCase();
	var isAdmob = packageLower == 'admob';
	var iosAdmobDetails = '''

		<preference name="IOS_APP_ID" />

		<config-file 
			target="*-Info.plist" 
			parent="GADApplicationIdentifier"
		>
			<string>\$IOS_APP_ID</string>
		</config-file>
''';
	var skAdnetworks = '';
	if (meta.skAdNetworks.isNotEmpty) {
		final networksString = meta.skAdNetworks.map((entry) {
			return '''
				<dict>
					<key>SKAdNetworkIdentifier</key>
					<string>$entry</string>
				</dict>''';
		});
		skAdnetworks = '''
		<config-file target="*-Info.plist" parent="SKAdNetworkItems">
			<array>
${networksString.join('\n')}
			</array>
		</config-file>''';
	}

	return '''
	<platform name="ios">${isAdmob ? iosAdmobDetails : ''}
		<podspec>
			<config>
				<source url="https://cdn.cocoapods.org/"/>
			</config>
			<pods>
				<pod 
					name="${meta.podName}" 
					spec="${meta.podVersion}" 
				/>
			</pods>
		</podspec>${skAdnetworks.isNotEmpty ? '\n$skAdnetworks' : ''}
	</platform>
''';
}
