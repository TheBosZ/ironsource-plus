class MediationMeta {
	final String name;
	final String version;
	final String podName;
	final String podVersion;
	final List<String> skAdNetworks;
	final List<String> mavenRepos;
	final List<String> dependencies;
	final List<String> api;

	MediationMeta(this.name, this.version, this.podName, this.podVersion, this.skAdNetworks,  this.mavenRepos, this.dependencies, this.api);

	bool get isAndroid {
		return dependencies.isNotEmpty;
	}

	bool get isIos {
		return podName != null;
	}
}
