import 'package:ironsource_plus_mediation_creator/mediation_meta.dart';

String buildGradleString(MediationMeta meta, String sdkVersion) {
	var extraRepo = meta.name.toLowerCase() == 'inmobi' ? '\n\tjcenter()' : '';
	var mavenStrings = meta.mavenRepos.map((url) {
		return 'maven { url "$url" }';
	});

	var depStrings = meta.dependencies.map((str) {
		if (str.startsWith('project(')) {
			return '\timplementation ($str)';
		}
		if (str.contains(r'$project')) {
			return '\timplementation "$str"';
		}
		return "\timplementation '$str'";
	}).toList();
	depStrings.addAll(meta.api.map((str) {
		return "\tapi '$str'";
	}));
	return '''
repositories {
	mavenCentral()
	maven { url "https://android-sdk.is.com/" }${mavenStrings.isNotEmpty ? '\n\t' : ''}${mavenStrings.join('\n')}$extraRepo
}

dependencies {
	implementation 'com.ironsource.sdk:mediationsdk:$sdkVersion'
${depStrings.join('\n')}
}
''';
}
