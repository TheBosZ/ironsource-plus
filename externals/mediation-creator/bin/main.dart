import 'dart:io';

import 'package:csv/csv.dart';
import 'package:csv/csv_settings_autodetection.dart';
import 'package:ironsource_plus_mediation_creator/ironsource_plus_mediation_creator.dart';
import 'package:ironsource_plus_mediation_creator/mediation_meta.dart';

void main(List<String> arguments) {
	//TODO Make this an argument or something
	var sdkVersion = '8.3.0';
	var data = parseCsv();
	print(data);
	for(var entry in data) {
		print('Creating files for ${entry[0]}');
		var meta = MediationMeta(
			entry[0],
			entry[1],
			entry[2],
			entry[3],
			(entry[4] as String).isNotEmpty ? (entry[4] as String).split(',') : [],
			(entry[5] as String).isNotEmpty ? (entry[5] as String).split(',') : [],
			(entry[6] as String).isNotEmpty ? (entry[6] as String).split(',') : [],
			(entry[7] as String).isNotEmpty ? (entry[7] as String).split(','): [],
		);
		
		var builtStrings = getAllStrings(meta, sdkVersion);
		var projectName = (entry[0] as String).toLowerCase();
		var baseDir = '../../packages/mediation-$projectName';
		Directory('$baseDir/src/android').createSync(recursive: true);
		File('$baseDir/plugin.xml').writeAsStringSync(builtStrings.pluginXML);
		File('$baseDir/package.json').writeAsStringSync(builtStrings.packageJson);
		if (meta.isAndroid && builtStrings.gradle != null) {
			File('$baseDir/src/android/adapter.gradle').writeAsStringSync(builtStrings.gradle);
		}
	}
}

List<List<dynamic>> parseCsv() {
	var dataString = File('data/details.csv').readAsStringSync();
	print(dataString);
	var data = CsvToListConverter(csvSettingsDetector: FirstOccurrenceSettingsDetector(eols: ['\r\n', '\n'])).convert(dataString);
	data.removeAt(0);
	return data;
}
